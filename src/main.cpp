#include <iostream>
#include <SDL2/SDL.h>
#include <stack>
#include <time.h>
#include "../include/Cell.h"
/*---------------------(Function Declaration)---------------------------*/
SDL_bool init(int argc, char** argv);
void render_cell(Cell* cell);
void _exit(int code);
void create_cells();
void add_neighbours();
void render();
void get_next();
void generate();
void clear();
/*-----------------------(const Declaration)---------------------------*/
int SCREEN_WIDTH = 301;
int SCREEN_HEIGHT = 301;
int CELL_WIDTH = 20;
int MAZE_WIDTH = SCREEN_WIDTH / CELL_WIDTH;
int MAZE_HEIGHT = SCREEN_HEIGHT / CELL_WIDTH;
/*-------------------(Global Variable Declaration)---------------------*/
SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
std::stack<Cell*> stack = std::stack<Cell*>();
Cell*** cells;
Cell* current;
Cell* next;
/*------------------------------(Main)---------------------------------*/
int main(int argc, char** argv){
    SDL_bool done = init(argc,argv);
    SDL_RenderPresent(renderer);
    cells = new Cell**[MAZE_HEIGHT];
    for(int row = 0; row < MAZE_HEIGHT; ++row){
        cells[row] = new Cell*[MAZE_WIDTH];
        for(int col = 0; col < MAZE_WIDTH; ++col){
            cells[row][col] = new Cell(row,col,CELL_WIDTH);
        }
    }
    create_cells();
    add_neighbours();
    current = cells[0][0];
    generate();
    SDL_Delay(10000);
    _exit(0);
}
/*----------------------(Function Implementation)---------------------------*/
void clear(){
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    for(int row = 0 ; row < MAZE_WIDTH; row++){
        for(int col = 0 ; col < MAZE_HEIGHT ; col++){
            cells[row][col]->clear(renderer);
        }
    }
    SDL_RenderPresent(renderer);
}
void generate(){
    stack.push(current);
    current->visit();
    while(stack.size() > 0){
        if(current->neigh_num() > 0) {
            get_next();
            if (next != nullptr) {
                stack.push(next);
                render_cell(current);
                render_cell(next);
                current = next;
            } else if ( stack.size() > 0){
                current = stack.top();
                stack.pop();
            }
        }
        else if ( stack.size() > 0){
            current = stack.top();
            stack.pop();
        }
    }
    std::cout << "DONE!!" << std::endl;
}
void render_cell(Cell* cell){
	cell->clear(renderer);
	cell->draw(renderer);
}
void get_next(){
    if(current->neigh_num() == 0 ){
        next = nullptr;
        return;
    }
    int random = rand() % current->neigh_num();
    next = current->get_neigh(random);
    if( next == nullptr ){
        return get_next();
    } else if( !(next->is_visited())){
        current->visit(next);
        current->neighbours.erase(current->neighbours.begin() + random);
    } else {
        current->neighbours.erase(current->neighbours.begin() + random);
        return get_next();
    }
}
void render(){
    for(int row = 0 ; row < MAZE_WIDTH; row++){
        for(int col = 0 ; col < MAZE_HEIGHT ; col++){
            render_cell(cells[row][col]);
        }
    }
    SDL_RenderPresent(renderer);
}
void add_neighbours(){
    Cell* temp;
    for(int row = 0 ; row < MAZE_WIDTH; row++){
        for(int col = 0 ; col < MAZE_HEIGHT ; col++){
            temp = cells[row][col];
            if( row > 0 && row < MAZE_HEIGHT - 1 ){
                temp->add_neighbour(cells[row-1][col]);
                temp->add_neighbour(cells[row+1][col]);
            } else if( row == 0 ){
                temp->add_neighbour(cells[row+1][col]);
            } else {
                temp->add_neighbour(cells[row-1][col]);
            }
            if( col > 0 && col < MAZE_WIDTH - 1 ){
                temp->add_neighbour(cells[row][col-1]);
                temp->add_neighbour(cells[row][col+1]);
            } else if( col == 0 ){
                temp->add_neighbour(cells[row][col+1]);
            } else {
                temp->add_neighbour(cells[row][col-1]);
            }
        }
    }
}
void create_cells(){
    for(int row = 0 ; row < MAZE_WIDTH; row++){
        for(int col = 0 ; col < MAZE_HEIGHT ; col++){
            cells[row][col] = new Cell(row,col,CELL_WIDTH);
        }
    }
}
void _exit(int code){
    for(int row = 0 ; row < MAZE_WIDTH; row++){
        for(int col = 0 ; col < MAZE_HEIGHT ; col++){
            delete cells[row][col];
        }
    }
    free(cells);
    if (renderer) {
        SDL_DestroyRenderer(renderer);
    }
    if (window) {
        SDL_DestroyWindow(window);
    }
    SDL_DestroyWindow( window );
    SDL_Quit();
    exit(code);
}
SDL_bool init(int argc, char** argv){
	    switch(argc){
        case 1: break;
        case 2:
            std::cout <<"Usage: maze_runner <hight = 10> <width = 10> <cell width = 10>" << std::endl;
            exit(0);
            break;
        case 3:
            MAZE_WIDTH = atoi(argv[2]);
            MAZE_HEIGHT = atoi(argv[1]);
            SCREEN_WIDTH = MAZE_WIDTH * CELL_WIDTH + 1;
            SCREEN_HEIGHT = MAZE_HEIGHT * CELL_WIDTH + 1;
            break;
        case 4:
            MAZE_WIDTH = atoi(argv[2]);
            MAZE_HEIGHT = atoi(argv[1]);
            CELL_WIDTH = atoi(argv[3]);
            SCREEN_WIDTH = MAZE_WIDTH * CELL_WIDTH + 1;
            SCREEN_HEIGHT = MAZE_HEIGHT * CELL_WIDTH + 1;
            break;
        default:
            std::cout <<"Usage: maze_runner <hight = 10> <width = 10> <cell width = 10>" << std::endl;
            exit(0);
    }
    srand(time(nullptr));
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){
        printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
        _exit(1);
    }
    if (SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, 0, &window, &renderer) < 0) {
        std::cout << MAZE_HEIGHT << "       " << MAZE_WIDTH << std::endl;
        return SDL_TRUE;
    }
    return SDL_FALSE;
}
