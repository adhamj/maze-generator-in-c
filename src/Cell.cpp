#include <iostream>
#include "../include/Cell.h"
Cell::Cell(int row, int col, int width){
    this->row = row;
    this->col = col;
    this->width = width;
    this->visited = false;
    this->neighbours = std::vector<Cell*>();
}
void Cell::visit(){
    this->visited = true;
}
bool Cell::is_visited(){
    return this->visited;
}
int Cell::neigh_num(){
    return neighbours.size();
}
Cell* Cell::get_neigh(int n){
    return (this->neighbours).at(n);
}
char* Cell::to_string() {
    std::cout << "{Cell("<<row<<"|"<<col<<") : visited : "<<this->visited<< std::endl;
}
void Cell::add_neighbour(Cell *neighbor){
    this->neighbours.push_back(neighbor);
}
void Cell::delete_wall(int n){
    this->walls[n] = false;
}
int Cell::get_col() {
    return this->col;
}
int Cell::get_row() {
    return this->row;
}
void Cell::visit(Cell* next) {
    next->visited = true;
    int n;
    if(next->get_col() > col){
        n = 3;
    } else if(next->get_col() < col){
        n = 1;
    } else if(row < next->get_row()){
        n = 2;
    }
    switch(n){
        case 0:
            this->delete_wall(0);
            next->delete_wall(2);
            break;
        case 1:
            this->delete_wall(1);
            next->delete_wall(3);
            break;
        case 2:
            this->delete_wall(2);
            next->delete_wall(0);
            break;
        case 3:
            this->delete_wall(3);
            next->delete_wall(1);
            break;
    }
}

void Cell::clear(SDL_Renderer* renderer){
    int x1 = col * width;
    int x2 = col * width + width;
    int y1 = row * width;
    int y2 = row * width + width;
    for(int i = 0 ; i < 4 ; i++) {
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderDrawLine(renderer, x1, y1, x2, y1);
        SDL_RenderDrawLine(renderer, x1, y1, x1, y2);
        SDL_RenderDrawLine(renderer, x1, y2, x2, y2);
        SDL_RenderDrawLine(renderer, x2, y2, x2, y1);
    }
}
void Cell::draw(SDL_Renderer* renderer){
    int x1 = col * width;
    int x2 = col * width + width;
    int y1 = row * width;
    int y2 = row * width + width;
    for(int i = 0 ; i < 4 ; i++){
        if(walls[i]) {
            SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
            switch (i) {
                case 0 : SDL_RenderDrawLine(renderer, x1, y1, x2, y1);break;
                case 1 :SDL_RenderDrawLine(renderer, x1, y1, x1, y2);break;
                case 2 :SDL_RenderDrawLine(renderer, x1, y2, x2, y2);break;
                case 3 :SDL_RenderDrawLine(renderer, x2, y2, x2, y1);break;
            }
        }
    }
    SDL_RenderPresent(renderer);
}
Cell::~Cell() {
    neighbours.clear();
}