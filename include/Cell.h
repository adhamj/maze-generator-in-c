#ifndef MAZE_CARVER_CELL_H
#define MAZE_CARVER_CELL_H
#include <SDL2/SDL.h>
#include <vector>
class Cell{
private:
    int row;
    int col;
    int width;
    bool visited;
    bool walls[4] =  {true,true,true,true}; /*indicates if was should be rendered*/
public:
    Cell();
    Cell(int row, int col, int width);
    Cell* get_neigh(int n);					/*gets neighbour to visit*/
    int get_row();
    int get_col();
    int neigh_num();						/*available neighbours, only non visited*/
    bool is_visited();
    char* to_string();
    std::vector<Cell*> neighbours;			/*a vector of neighbours*/
    void add_neighbour(Cell *neighbor);
    void visit(Cell* next);					/*marks visited & deletes walls between cells*/
    void visit();
    void delete_wall(int n);
    void clear(SDL_Renderer* renderer);
    void draw(SDL_Renderer* renderer);
    ~Cell();
};



#endif //MAZE_CARVER_CELL_H
