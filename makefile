all: main


main: bin/main.o bin/cell.o
	g++ -o bin/main bin/main.o bin/cell.o   -lSDL2

bin/main.o: src/main.cpp
	g++ -g -Wall -Weffc++ -std=c++11 -c  -o  bin/main.o src/main.cpp  -lSDL2

bin/cell.o: src/Cell.cpp
	g++ -g -Wall -Weffc++ -std=c++11 -c  -o  bin/cell.o src/Cell.cpp  -lSDL2

#bin/maze.o: src/Maze.cpp
	#g++ -g -Wall -Weffc++ -std=c++11 -c  -o  bin/maze.o src/Maze.cpp  -lSDL2

clean:
	rm -f bin/*
